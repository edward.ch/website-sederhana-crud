<?php
session_start();
include('../../config/database.php');

if($_SESSION['status'] != 'login'){
    header("location:login.php?pesan=belum_login");
}

if(isset($_POST['tambah'])){

    $nama           = $_POST['nama'];
    $harga          = $_POST['harga'];
    $deskripsi      = $_POST['deskripsi'];

    $ekstensi       = ['jpg','jpeg','png'];
    $namaFile       = $_FILES['gambar']['name'];
    $ukuran         = $_FILES['gambar']['size'];
    $maksUkuran     = 1044070;
    $ext            = pathinfo($namaFile, PATHINFO_EXTENSION);

    if(!in_array($ext, $ekstensi)){

        header("location:tambah.php?pesan=gagal_gambar");

    }else{

        if($ukuran < $maksUkuran){
            
            $namaGambar = date("Ymdhis")."_".$namaFile;
            move_uploaded_file($_FILES['gambar']['tmp_name'], '../../assets/img/'.$namaGambar);

        }else{
            header("location:tambah.php?pesan=gagal_gambar");
        }

    }

    $sql            = "INSERT INTO barang (nama, harga, deskripsi, gambar) VALUE ('$nama','$harga','$deskripsi','$namaGambar')";
    $query          = mysqli_query($db, $sql);

    if($query){
        header("location:daftar.php?pesan=sukses");
    }else{
        header("location:tambah.php?pesan=gagal");
    }



}else{
    die('Akses Dilarang!!!');
}


?>