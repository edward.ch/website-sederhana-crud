<?php
    session_start();
    include('../../config/database.php');

    if($_SESSION['status'] != 'login'){
        header("location:login.php?pesan=belum_login");
    }

?>
<!doctype html>
<html lang="en">
    <head>

        <!-- Koneksi CSS -->
        <link rel="stylesheet" type="text/css" href="style.css">
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

        <title>Halaman Daftar Data</title>

    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="#">Halaman Admin</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                    <a class="nav-link" href="daftar.php">Barang <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="../proses-logout.php">Logout</a>
                    </li>
                </ul>
                </div>
            </div>
        </nav>

        <div class="container">
            <h1 class="mt-4">Table</h1>
            <a href="tambah.php" class="btn btn-primary mt-4">Tambah Data</a>
            <?php
                if(isset($_GET['pesan'])){
            ?>
                <div class="alert alert-success alert-dismissible fade show mt-2" role="alert">
                    <?php
                        if($_GET['pesan'] == 'sukses'){

                            echo "Data Berhasil Ditambahkan";

                        }
                    ?>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            <?php
                }
            ?>
            <table class="table mt-2">
                <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Gambar</th>
                    <th scope="col">Nama</th>
                    <th scope="col">Harga</th>
                    <th scope="col">Aksi</th>
                </tr>
                </thead>
                <tbody>
                    <?php

                    $sql    = "SELECT * FROM barang";
                    $query  = mysqli_query($db, $sql);

                    $i = 1;
                    while($barang = mysqli_fetch_array($query)) {
                    ?>
                    <tr>
                        <th scope="row"><?php echo $i ?></th>
                        <td><img src="../../assets/img/<?php echo $barang['gambar'] ?>" width="100" alt=""></td>
                        <td><?php echo $barang['nama'] ?></td>
                        <td>Rp. <?php echo $barang['harga'] ?></td>
                        <td>
                            <div class="btn-group" aria-label="Basic example">
                                <a href="edit.php?id=<?php echo $barang['id'] ?>" class="btn btn-primary btn-sm">Edit</a>
                                <a href="proses-hapus.php?id=<?php echo $barang['id'] ?>" class="btn btn-danger btn-sm" 
                                onclick="return confirm('Apakah anda yakin menghapus data ini?')">Hapus</a>
                            </div>
                        </td>
                    </tr>
                    <?php
                    $i++;
                    }
                    ?>

                </tbody>
            </table>
        </div>
        
        <!-- Optional JavaScript; choose one of the two! -->

        <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

        <!-- Option 2: Separate Popper and Bootstrap JS -->
        <!--
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        -->
    </body>
    </html>