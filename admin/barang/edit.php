<?php
    session_start();
    include('../../config/database.php');

    if($_SESSION['status'] != 'login'){
        header("location:login.php?pesan=belum_login");
    }

    if(isset($_GET['id'])){

        $id         = $_GET['id'];

        $sql        = "SELECT * FROM barang WHERE id='$id'";
        $query      = mysqli_query($db, $sql);
        $barang     = mysqli_fetch_assoc($query);

        if(!$barang){
            die('Data Tidak Ditemukan');
        }

    }else{
        die('Akses Dilarang');
    }



?>
<!doctype html>
<html lang="en">
    <head>

        <!-- Koneksi CSS -->
        <link rel="stylesheet" type="text/css" href="style.css">
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">

        <title>Halaman Edit Data</title>
    </head>
    <body>
    
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <div class="container">
                <a class="navbar-brand" href="#">Halaman Edit</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                    <a class="nav-link" href="daftar.php">Barang <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" href="../proses-logout.php">Logout</a>
                    </li>
                </ul>
                </div>
            </div>
        </nav>

        <div class="auth">
            <div class="container">
                <div class="d-flex justify-content-center">
                    <div class="col-md-6 col-md-5">
                        <div class="card">
                            <div class="card-body">
                            <?php
                                    if(isset($_GET['pesan'])){
                                ?>
                                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                        <?php
                                            if($_GET['pesan'] == 'gagal'){

                                                echo "Edit Data Gagal! Cek Kembali Fieldnya";

                                            }elseif($_GET['pesan'] == 'gagal_gambar'){

                                                echo "Format Gambar yang Diupload Tidak Sesuai";

                                            }
                                        ?>
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                <?php
                                    }
                                ?>
                                <form action="proses-edit.php?id=<?php echo $barang['id']; ?>" method="POST" enctype="multipart/form-data" >
                                    <h3>Tambah Data</h3>
                                    <div class="form-group mt-4">
                                        <input type="text" name="nama" value="<?php echo $barang['nama']; ?>" class="form-control" placeholder="Nama Barang...." required>
                                    </div>
                                    <div class="form-group mt-4">
                                        <input type="number" name="harga" value="<?php echo $barang['harga']; ?>" class="form-control" placeholder="Harga Barang...." required>
                                    </div>
                                    <div class="form-group mt-4">
                                        <input type="text" name="deskripsi" value="<?php echo $barang['deskripsi']; ?>" class="form-control" placeholder="Deskripsi Barang...." required>
                                    </div>
                                    <div class="form-group">
                                        <input type="file" name="gambar" class="form-control" id="exampleInputPassword1" placeholder="Tambah Gambar">
                                    </div>
                                    <input type="hidden" name="gambar_lama" value="<?php echo $barang['gambar']; ?>">

                                    <button type="submit" name="edit" class="btn btn-primary">Tambahkan Data</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Optional JavaScript; choose one of the two! -->

        <!-- Option 1: jQuery and Bootstrap Bundle (includes Popper) -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>

        <!-- Option 2: Separate Popper and Bootstrap JS -->
        <!--
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>
        -->
    </body>
    </html>

